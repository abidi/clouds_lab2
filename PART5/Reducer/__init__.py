# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

"""
The reducer should take as input a key-value pair, where 
    - key is a word 
    - value is a list (which will be all 1s)
The reducer should add up all the values in the list and produce as output a key—value pair where 
    - key is word 
    - value is total count
"""
def main(input: dict) -> dict:
    result = {}
    for word in input.keys():
        result[word]=len(input[word])
    return(result)

