# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

from azure.storage.blob import BlobServiceClient
from azure.storage.blob.aio import BlobClient
import os

# GetInputDataFn uses the blob store API and connection string to pull down the files from Azure. 
# It should then read each file, break it into lines, and build the overall input to mapreduce which you faked earlier – a list of [<offset, line string>, …] key-value pairs.

CONNECTIION_STRING = "DefaultEndpointsProtocol=https;AccountName=abidimapreduce;AccountKey=ORvjBWIXBo4seKdqjp0K41lYje8uYJ/+v3uQd+mHY/9hwJhvqUMOpDgEhNysg6AEYWtKFcFxolAd+ASt2h40vQ==;EndpointSuffix=core.windows.net"
CONTAINER_NAME = "abidimapreducecontainer"

def read_file(filename: str) -> list :
    # Retrieve the connection string for use with the application. The storage
    # connection string is stored in an environment variable on the machine
    # running the application called AZURE_STORAGE_CONNECTION_STRING. If the environment variable is
    # created after the application is launched in a console or with Visual Studio,
    # the shell or application needs to be closed and reloaded to take the
    # environment variable into account.
    blob_service_client = BlobServiceClient.from_connection_string(CONNECTIION_STRING)
    container_client = blob_service_client.get_container_client(container=CONTAINER_NAME)
    blob_client = container_client.get_blob_client(blob=filename)
    stream = blob_client.download_blob().readall()
    
    lines = []
    for index, line in enumerate(stream.decode().splitlines()):
        lines.append([index, line])
    return lines


def main(filename: str) -> list:
    return read_file(filename)
