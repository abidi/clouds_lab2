# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

"""
The mapper function should take as input a <key, value> pair, where: 
    - key: the line number 
    - value: the line string
It then produces as output a list of key—value pairs where 
    - key is word 
    - value is 1 
The mapper tokenize the line into words and reduce <word, 1> pairs.
"""

def main(line) -> list:
    mapped_data = []
    words = line[1].split()
    mapped_data = [(w,1) for w in words]
   
    return(mapped_data)


