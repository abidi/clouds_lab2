from locust import HttpUser, task, between
class MyUser(HttpUser):
    @task
    def numerical_integration_task(self):
        self.client.get("/api/httpexample?l=0&u=3.14159")

