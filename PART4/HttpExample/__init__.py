import logging
import azure.functions as func
import math

def numericalIntegral(l, u):
  result = {}
  num_intervals=[10,100,1000,10000,100000]

  for N in num_intervals:
    delta_x = (float(u) - float(l))/N
    integral_sum = 0
    for i in range(N):
      x_i = float(l) + delta_x*i
      integral_sum+= abs(math.sin(x_i))*delta_x
    
    result[N] = integral_sum

  return result

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    l = req.params.get('l')
    u = req.params.get('u')
    
    if u and l:
        integral = numericalIntegral(float(l), float(u))
        return func.HttpResponse(f"The result of the numerical integral is. {integral}")
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
             status_code=200
        )
