import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Read the CSV files for PART1 , PART2, PART3 and PART4
input_1 = pd.read_csv('locust_results_csv/PART1/locust_out_stats_history.csv')
input_2 = pd.read_csv('locust_results_csv/PART2/locust_out_stats_history.csv')
input_3 = pd.read_csv('locust_results_csv/PART3/locust_out_stats_history.csv')
input_4 = pd.read_csv('locust_results_csv/PART4/locust_out_stats_history.csv')

input_1['Timestamp'] = pd.to_datetime(input_1['Timestamp'], unit='s')


# Plot RPS over time for the 4 parts
plt.figure(figsize=(10, 5))

# Set y-axis scale to logarithmic
plt.yscale('log')

plt.plot(input_1['Timestamp'], input_1['Requests/s'], label='PART1')

plt.plot(input_1['Timestamp'], input_2['Requests/s'], label='PART2')

plt.plot(input_1['Timestamp'], input_3['Requests/s'], label='PART3')

plt.plot(input_1['Timestamp'], input_4['Requests/s'], label='PART4')


# Plots
plt.title('Locust Metrics')
plt.xlabel('Timestamp')
plt.ylabel('Counts')
plt.legend()
plt.grid(True)

# Rotation for better reading of timestamps
plt.xticks(rotation=45)

# Show the plot
plt.tight_layout()
plt.show()