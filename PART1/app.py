
from flask import Flask, request, jsonify
import math

app = Flask(__name__)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

@app.route('/')
def start():
    return 'Welcome to the Numerical Integration Microservice'

@app.route('/numericalintegralservice/<lower>/<upper>', methods=['GET'])
def numerical_integral_service(lower, upper):
    lower = float(lower)
    upper = float(upper)
    num_intervals = [10, 100, 1000, 10000, 100000, 1000000]
    results = {}

    for N in num_intervals:
        result = numerical_integration(lower, upper, N)
        results[N] = result

    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=True)

