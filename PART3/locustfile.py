from locust import HttpUser, task, between
class MyUser(HttpUser):
    @task
    def numerical_integration_task(self):
        self.client.get("/numericalintegralservice/0/3.14159")
